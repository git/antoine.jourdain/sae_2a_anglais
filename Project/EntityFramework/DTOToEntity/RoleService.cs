﻿using DbContextLib;
using DTO;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOToEntity
{
    public class RoleService : IService<RoleDTO>
    {
        private readonly UnitOfWork _context = new UnitOfWork();

        public RoleService() { }
        public RoleService(StubbedContext context)
        {
            _context = new UnitOfWork(context);
        }

        public RoleService(UnitOfWork context)
        {
            _context = context;
        }
        public async Task<RoleDTO> Add(RoleDTO role)
        {
            var roleEntity = role.ToEntity();
            _context.RoleRepository.Insert(roleEntity);
            await _context.SaveChangesAsync();
            return role;
        }

        public async Task<RoleDTO> Delete(object id)
        {
            RoleEntity role = _context.RoleRepository.GetById((long)id);
            if (role == null)
            {
                throw new Exception("Role not found");
            }
            _context.RoleRepository.Delete((long)id);
            await _context.SaveChangesAsync();
            return role.ToDTO();
        }

        public async Task<RoleDTO> GetById(object id)
        {
            RoleEntity? role = _context.RoleRepository.GetById((long)id);
            if (role == null)
            {
                throw new Exception("Role not found");
            }
            return role.ToDTO();
        }

        public async Task<PageResponse<RoleDTO>> Gets(int index, int count)
        {
           IEnumerable<RoleEntity> roles = _context.RoleRepository.GetItems(index, count);
            return new PageResponse<RoleDTO>(roles.ToList().Select(r => r.ToDTO()), _context.RoleRepository.GetItems(0,1000000000).Count());
        }

        public async Task<RoleDTO> Update(RoleDTO role)
        {
            if (role == null)
            {
                throw new ArgumentNullException();
            }
            var roleEntity = _context.RoleRepository.GetById(role.Id);
            if (roleEntity != null)
            {
                throw new Exception("role not found");
            }
            roleEntity.Name = role.Name;
            _context.RoleRepository.Update(roleEntity);
            await _context.SaveChangesAsync();
            return roleEntity.ToDTO();
        }
    }
}