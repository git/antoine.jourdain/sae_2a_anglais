﻿using DbContextLib;
using DTO;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOToEntity
{
    public class TranslateService : ITranslateService
    {
        private readonly UnitOfWork _context = new UnitOfWork();

        public TranslateService() { }
        public TranslateService(StubbedContext context)
        {
            _context = new UnitOfWork(context);
        }

        public async Task<TranslateDTO> Add(TranslateDTO translate)
        {
            var translateEntity = translate.ToEntity();
            _context.TranslateRepository.Insert(translateEntity);
            await _context.SaveChangesAsync();
            return translateEntity.ToDTO();

        }

        public async Task<VocabularyDTO> AddVocabToTranslate(string vocabId, long translateId)
        {
            var vocab = _context.VocabularyRepository.GetById(vocabId);
            if (vocab == null)
            {
                throw new Exception("Vocabulary not found");
            }
            var translate = _context.TranslateRepository.GetById(translateId);
            if (translate == null)
            {
                throw new Exception("Translate not found");
            }
            translate.TransVoc.Add(vocab);
            await _context.SaveChangesAsync();
            return vocab.ToDTO();
        }

        public async Task<TranslateDTO> Delete(object id)
        {
            var translate = _context.TranslateRepository.GetById((long)id);
            if (translate != null)
            {
                _context.TranslateRepository.Delete(translate);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("Translate not found");
            }
            return translate.ToDTO();
        }

        public async Task<TranslateDTO> GetById(object id)
        {
            var translate = _context.TranslateRepository.GetById((long)id);
            if (translate == null)
            {
                throw new Exception("Translate not found");
            }
            return translate.ToDTO();
        }

        public async Task<PageResponse<TranslateDTO>> Gets(int index, int count)
        {
            var translates = _context.TranslateRepository.GetItems(index, count);
            if(translates == null)
            {
                throw new Exception("No translates found");
            }
            return new PageResponse<TranslateDTO>( translates.Select(t => t.ToDTO()), _context.TranslateRepository.GetItems(0, 100000000).Count());
        }

        public async Task<TranslateDTO> Update(TranslateDTO translate)
        {
            var translateEntity = _context.TranslateRepository.GetById(translate.Id);
            if (translateEntity == null)
            {
                throw new Exception("Translate not found");
            }
            translateEntity.WordsId = translate.WordsId;
            translateEntity.VocabularyListVocId = translate.VocabularyListVocId;
            _context.TranslateRepository.Update(translateEntity);
            await _context.SaveChangesAsync();
            return translateEntity.ToDTO();
        }
    }
}
