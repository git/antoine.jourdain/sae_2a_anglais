﻿using DbContextLib;
using DTO;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOToEntity
{
    public class VocabularyListService : IVocabularyListService
    {
        private UnitOfWork _context = new UnitOfWork();

        public VocabularyListService()
        {
        }

        public VocabularyListService(StubbedContext context)
        {
            _context = new UnitOfWork(context);
        }
        public async Task<VocabularyListDTO> Add(VocabularyListDTO group)
        {
            var groupEntity = group.ToEntity();
            _context.VocabularyListRepository.Insert(groupEntity);
            await _context.SaveChangesAsync();
            return groupEntity.ToDTO();
        }

        public async Task<GroupDTO> AddGroupToVocabularyList(long groupId, long vocabId)
        {
            var group = _context.GroupRepository.GetById(groupId);
            if (group == null)
            {
                throw new Exception("Group not found");
            }
            var vocab = _context.VocabularyListRepository.GetById(vocabId);
            if (vocab == null)
            {
                throw new Exception("Vocabulary List not found");
            }
            vocab.VocsGroups.Add(group);
            await _context.SaveChangesAsync();
            return group.ToDTO();

        }

        public async Task<VocabularyListDTO> Delete(object id)
        {
            var group =  _context.VocabularyListRepository.GetById((long)id);
            if (group != null)
            {
                _context.VocabularyListRepository.Delete(group);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new Exception("Group not found");
            }
            return group.ToDTO();
        }

        public async Task<VocabularyListDTO> GetById(object id)
        {
            var group =  _context.VocabularyListRepository.GetById((long)id);
            if (group == null)
            {
                throw new Exception("Group not found");
            }
            return group.ToDTO();
        }

        public async Task<PageResponse<VocabularyListDTO>> GetByUser(int index, int count, int user)
        {
            var groups = _context.VocabularyListRepository.GetItems(filter: u => u.Id == user, index, count);
            return new PageResponse<VocabularyListDTO>(groups.Select(g => g.ToDTO()), _context.VocabularyListRepository.GetItems(0,1000000000).Count());

        }

        public async Task<PageResponse<VocabularyListDTO>> Gets(int index, int count)
        {
            var groups = _context.VocabularyListRepository.GetItems(index, count);
            return new PageResponse<VocabularyListDTO>(groups.Select(g => g.ToDTO()), _context.VocabularyListRepository.GetItems(0, 1000000000).Count());
        }

        public async Task<VocabularyListDTO> Update(VocabularyListDTO group)
        {
            var groupEntity = group.ToEntity();
            if (groupEntity == null)
            {
                throw new Exception("Group Entity is null");
            }
            _context.VocabularyListRepository.Update(groupEntity);
            await _context.SaveChangesAsync();
            return group;
        }
    }
}
