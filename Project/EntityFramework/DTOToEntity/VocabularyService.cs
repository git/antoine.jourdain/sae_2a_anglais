﻿using DbContextLib;
using DTO;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOToEntity
{
    public class VocabularyService : IVocabularyService
    {
        private readonly UnitOfWork _context = new UnitOfWork();

        public VocabularyService() { }

        public VocabularyService(StubbedContext context)
        {
            _context = new UnitOfWork(context);
        }
        public async Task<VocabularyDTO> Add(VocabularyDTO vocabulary)
        {
           var vocabularyEntity = vocabulary.ToEntity();
            if(vocabularyEntity == null)
            {
                throw new ArgumentNullException();
            }
           _context.VocabularyRepository.Insert(vocabularyEntity);
           await _context.SaveChangesAsync();
           return vocabularyEntity.ToDTO();
        }

        public async Task<TranslateDTO> AddTranslationToVocabulary(string vocabId, long translateId)
        {
            var vocabulary = _context.VocabularyRepository.GetById(vocabId);
            if(vocabulary == null)
            {
                throw new Exception("Vocabulary not found");
            }
            var translate = _context.TranslateRepository.GetById(translateId);
            if(translate == null)
            {
                throw new Exception("Translate not found");
            }
            vocabulary.Voctranslations.Add(translate);
            await _context.SaveChangesAsync();
            return translate.ToDTO();
        }

        public async Task<VocabularyDTO> Delete(object id)
        {
            var vocabulary = _context.VocabularyRepository.GetById((string)id);
            if(vocabulary == null)
            {
                throw new Exception("Vocabulary not found");
            }
            _context.VocabularyRepository.Delete(vocabulary);
            await _context.SaveChangesAsync();
            return vocabulary.ToDTO();
        }

        public async Task<VocabularyDTO> GetById(object id)
        {
            var vocabulary = _context.VocabularyRepository.GetById((string)id);
            if(vocabulary == null)
            {
                throw new Exception("Vocabulary not found");
            }
            return vocabulary.ToDTO();
        }

        public async Task<PageResponse<VocabularyDTO>> GetByLangue(int index, int count, string langue)
        {
            var vocabularies = _context.VocabularyRepository.GetItems(filter: v => v.LangueName == langue, index, count);
            return new PageResponse<VocabularyDTO>(vocabularies.ToList().Select(v => v.ToDTO()), _context.VocabularyRepository.GetItems(0,100000000).Count());

        }

        public async Task<PageResponse<VocabularyDTO>> Gets(int index, int count)
        {
            var vocabulary = _context.VocabularyRepository.GetItems(index, count);
            return new PageResponse<VocabularyDTO>(vocabulary.Select(v => v.ToDTO()), _context.VocabularyRepository.GetItems(0, 100000000).Count());
        }

        public async Task<VocabularyDTO> Update(VocabularyDTO vocabulary)
        {
            VocabularyEntity vocabularyEntity = vocabulary.ToEntity();
            if(vocabularyEntity == null)
            {
                throw new Exception("vocabulary not valid");
            }
            var VocabToUpdate = _context.VocabularyRepository.GetById(vocabulary.word);
            if(VocabToUpdate == null)
            {
                throw new Exception("vocabulary not found");
            }
           VocabToUpdate.LangueName = vocabularyEntity.LangueName;
            await _context.SaveChangesAsync();
            return vocabularyEntity.ToDTO();
        }
    }
}
