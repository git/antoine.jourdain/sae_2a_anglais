﻿// See https://aka.ms/new-console-template for more information
using DbContextLib;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;


Console.WriteLine("Hello, World!");


var connection = new SqliteConnection("DataSource=:memory:");
connection.Open();
var options = new DbContextOptionsBuilder<SAEContext>()
                    .UseSqlite(connection)
                    .Options;
Console.WriteLine("Test Users : \n\n");
Console.WriteLine("\n\nTest Users : \n\n");
using (var uow = new UnitOfWork(options))
{
    var users = uow.UserRepository;
    var user1 = new UserEntity
    {
        Name = "name4",
        UserName = "username4",
        NickName = "nickname4",
        ExtraTime = true,
        GroupId = 1,
        Password = "12344",
        Email = "",
        RoleId = 3,
        image = "image4",
    };

    users.Insert(user1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest ajout (1 user supplémentaire normalement) \n");
    foreach (var user in users.GetItems(0, 5))
    {
        Console.WriteLine(user.toString());
    }

    user1.Name = "updated";
    users.Update(user1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest update (le nom doit être 'updated') \n");
    Console.WriteLine(user1.toString());

    users.Delete(user1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest suppression\n");
    foreach (var user in users.GetItems(0, 5))
    {
        Console.WriteLine(user.toString());
    }
}

Console.WriteLine("\n\nTest VocsGroups : \n\n");
using (var uow = new UnitOfWork(options))
{
    var groups = uow.GroupRepository;
    var users = uow.UserRepository;
    var user1 = new UserEntity
    {
        Name = "name4",
        UserName = "username4",
        NickName = "nickname4",
        ExtraTime = true,
        GroupId = 1,
        Password = "12344",
        Email = "",
        RoleId = 3,
        image = "image4",
    };
    var group1 = new GroupEntity
    {
        Num = 4,
        year = 4,
        sector = "sector4",
        Users = new List<UserEntity> { user1 }
    };

    groups.Insert(group1);
    await uow.SaveChangesAsync();

    Console.WriteLine("\ntest ajout (1 group supplémentaire normalement) \n");
    foreach (var group in groups.GetItems(0, 5))
    {
        Console.WriteLine(group.toString());
    }

    group1.Num = 5;
    group1.sector = "updated";
    groups.Update(group1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest update (le nom doit être 'updated' et le Num = 5) \n");
    Console.WriteLine(group1.toString());

    Console.WriteLine("\n test utilisateur du groupe normalement le user : updated ");
    foreach (var group in groups.GetItems(0, 5))
    {
        foreach (var user in users.GetItems(0, 5))
        {
            Console.WriteLine(user.toString());
        }
    }
    groups.Delete(group1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest suppression\n");
    foreach (var group in groups.GetItems(0, 5))
    {
        Console.WriteLine(group.toString());
    }
}

Console.WriteLine("\n\nTest Langue : \n\n");
using (var uow = new UnitOfWork(options))
{
    var langues = uow.LangueRepository;

    Console.WriteLine("\ntest show 5 first Langues : \n");
    foreach (var langue in langues.GetItems(0, 5))
    {
        Console.WriteLine(langue.name);
    }

    Console.WriteLine("\n ajout Langue (normalement chinese\n");
    var langue1 = new LangueEntity
    {
        name = "Chinese"
    };
    langues.Insert(langue1);
    await uow.SaveChangesAsync();
    foreach (var langue in langues.GetItems(0, 5))
    {
        Console.WriteLine(langue.name);
    }

    Console.WriteLine("\n test des liens langues <=> vocabulaires (resultat attendu : 'word1 Chinese'\n");
    var vocabularyList1 = new VocabularyEntity
    {
        word = "word1",
        LangueName = "Chinese",
        Langue = langue1
    };
    uow.VocabularyRepository.Insert(vocabularyList1);
    await uow.SaveChangesAsync();
    foreach (var langue in langues.GetItems(0, 5))
    {
        foreach (var vocabularyList in langue.vocabularys)
        {
            Console.WriteLine(vocabularyList.toString());
        }
    }
    langues.Delete(langue1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest suppression (il n'y a normalement plus la langue 'Chinese'\n");
    foreach (var langue in langues.GetItems(0, 5))
    {
        Console.WriteLine(langue.name);
    }
}

Console.WriteLine("\n\nTest Roles : \n\n");
using (var uow = new UnitOfWork(options))
{
    var roles = uow.RoleRepository;

    Console.WriteLine("\ntest show 5 first Roles : \n");
    foreach (var role in roles.GetItems(0, 5))
    {
        Console.WriteLine(role.toString());
    }

    var role1 = new RoleEntity
    {
        Name = "Role4"
    };
    roles.Insert(role1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\n ajout Role (normalement Role4\n");
    foreach (var role in roles.GetItems(0, 5))
    {
        Console.WriteLine(role.toString());
    }

    role1.Name = "updated";
    roles.Update(role1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest update (le nom doit être 'updated') \n");
    Console.WriteLine(role1.toString());

    Console.WriteLine("\n test utilisateur du role normalement");
    foreach (var role in roles.GetItems(0, 5))
    {
        foreach (var user in role.Users)
        {
            Console.WriteLine(user.toString());
        }
    }
    roles.Delete(role1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest suppression (il n'y a normalement plus le role 'Role4'\n");
    foreach (var role in roles.GetItems(0, 5))
    {
        Console.WriteLine(role.toString());
    }
}

Console.WriteLine("\n\nTest Translate : \n\n");
using (var uow = new UnitOfWork(options))
{
    var translates = uow.TranslateRepository;

    Console.WriteLine("\ntest show 5 first Translates : \n");
    foreach (var translate in translates.GetItems(0, 5))
    {
        Console.WriteLine(translate.toString());
    }

    var translate1 = new TranslateEntity
    {
        WordsId = "Bonjour",
        VocabularyListVocId = 1
    };
    translates.Insert(translate1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\n ajout Translate (normalement word4\n");
    foreach (var translate in translates.GetItems(0, 5))
    {
        Console.WriteLine(translate.toString());
    }

    var translate2 = new VocabularyEntity
    {
        word = "word4",
        LangueName = "English",
    };
    uow.VocabularyRepository.Insert(translate2);
    await uow.SaveChangesAsync();
    translate1.WordsId = "word4";
    translates.Update(translate1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest update (le mot doit être 'updated') \n");
    Console.WriteLine(translate1.toString());

    translates.Delete(translate1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest suppression (il n'y a normalement plus le mot 'word4'\n");
    foreach (var translate in translates.GetItems(0, 5))
    {
        Console.WriteLine(translate.toString());
    }
}

Console.WriteLine("\n\nTest Vocabulary : \n\n");
using (var uow = new UnitOfWork(options))
{
    var vocabularies = uow.VocabularyRepository;

    Console.WriteLine("\ntest show 5 first Vocabularys : \n");
    foreach (var vocabulary in vocabularies.GetItems(0, 5))
    {
        Console.WriteLine(vocabulary.toString());
    }

    var vocabulary1 = new VocabularyEntity
    {
        word = "NewWord",
        LangueName = "English"
    };
    vocabularies.Insert(vocabulary1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\n ajout Vocabulary (normalement NewWord) \n");
    foreach (var vocabulary in vocabularies.GetItems(0, 5))
    {
        Console.WriteLine(vocabulary.toString());
    }

    vocabulary1.LangueName = "French";
    vocabularies.Update(vocabulary1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest update (la langue doit être 'French') \n");
    Console.WriteLine(vocabulary1.toString());

    vocabularies.Delete(vocabulary1);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest suppression (il n'y a normalement plus le mot 'NewWord'\n");
    foreach (var vocabulary in vocabularies.GetItems(0, 5))
    {
        Console.WriteLine(vocabulary.toString());
    }
}

Console.WriteLine("\n\nTest GroupVocabularyList : \n\n");
using (var uow = new UnitOfWork(options))
{
    var vocabularyLists = uow.VocabularyListRepository;

    Console.WriteLine("\ntest show 5 first VocabularyLists : \n");
    foreach (var vocabularyList in vocabularyLists.GetItems(0, 5))
    {
        Console.WriteLine(vocabularyList.toString());
    }

    var vocabularyList2 = new VocabularyListEntity
    {
        Name = "name4",
        Image = "image4",
        UserId = 3
    };
    vocabularyLists.Insert(vocabularyList2);
    await uow.SaveChangesAsync();
    Console.WriteLine("\n ajout GroupVocabularyList (normalement name4) \n");
    foreach (var vocabularyList in vocabularyLists.GetItems(0, 5))
    {
        Console.WriteLine(vocabularyList.toString());
    }

    vocabularyList2.Name = "updated";
    vocabularyLists.Update(vocabularyList2);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest update (le nom doit être 'updated') \n");
    Console.WriteLine(vocabularyList2.toString());

    Console.WriteLine("\n test affichage des owner (user) des listes normalement");
    foreach (var vocabularyList in vocabularyLists.GetItems(0, 5))
    {
        if(vocabularyList.User != null)
        Console.WriteLine(vocabularyList.User.toString());
    }

    vocabularyLists.Delete(vocabularyList2);
    await uow.SaveChangesAsync();
    Console.WriteLine("\ntest suppression (il n'y a normalement plus la liste 'name4'\n");
    foreach (var vocabularyList in vocabularyLists.GetItems(0, 5))
    {
        Console.WriteLine(vocabularyList.toString());
    }
}


